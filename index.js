var Adapters = require('ancient-adapters').Adapters;

/** prototype **/

exports.DocumentConstructor = function() {};

exports.documentPrototype = new exports.DocumentConstructor();

// new (adapters: Adapters, factoryDocumentConstructor: Function)
var Factory = function(adapters, factoryDocumentConstructor) {
  var factory = this;
  
  this.adapters = adapters;
  
  adapters.documentFactory = this;
  
  this.DocumentConstructor = factoryDocumentConstructor;
  if (typeof(this.DocumentConstructor) != 'function') {
    this.DocumentConstructor = function FactoryDocumentConstructor() {};
  }
  this.DocumentConstructor.prototype = exports.documentPrototype;
  this.documentPrototype = new this.DocumentConstructor(this);
  
  // { adapterName: Object }
  this.adaptersDocumentPrototypes = {};
  
  // { adapterName: new (factory: Factory) }
  this.adaptersDocumentConstructors = {};
};

// (adapterName: String, data: any) => document?: Document
Factory.prototype.new = function(adapterName, data) {
  var factory = this;
  var adapters = this.adapters;
  
  if (!this.adaptersDocumentConstructors[adapterName]) {
    var adapter = this.adapters.get(adapterName);
    
    if (!adapter) {
      throw new Error('Adapter "'+adapterName+'" is not defined.');
    }
    
    if (typeof(adapter.DocumentConstructor) == 'function') {
      adapter.DocumentConstructor.prototype = this.documentPrototype;
      this.adaptersDocumentPrototypes[adapterName] = new adapter.DocumentConstructor(this);
    } else {
      var adapterDocumentConstructor = function AdapterDocumentConstructor() {};
      adapterDocumentConstructor.prototype = this.documentPrototype;
      this.adaptersDocumentPrototypes[adapterName] = new adapterDocumentConstructor(this);
    }
    
    this.adaptersDocumentConstructors[adapterName] = function Document(data) {
      this.adapters = adapters;
      this.adapterName = adapterName;
      this.adapter = adapter;
      this.data = data;
      this.factory = factory;
    };
    this.adaptersDocumentConstructors[adapterName].prototype = this.adaptersDocumentPrototypes[adapterName];
  }
  
  return new this.adaptersDocumentConstructors[adapterName](data);
};

exports.Factory = Factory;