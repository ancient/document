# Document^0.0.3 ^[wiki](https://gitlab.com/ancient/document/wikis/home)

Unified operations with one data item based on single adapter.

## Theory

###### `object` adapters

It is a object with user adapted methods of any of databases for some class.

A detailed description of the requirements to the adapter can be found in the [wiki](https://gitlab.com/ancient/document/wikis/home).

[Adapters repository](https://gitlab.com/ancient/adapters).

###### `class` Factory

![Chart](https://gitlab.com/ancient/document/raw/master/chart.png)

## Example

```js
var Adapters = require('ancient-adapters').Adapters;

// RootDocumentConstructor
require('ancient-document').documentPrototype.a = 1;

var AdapterDocumentConstructor = function() {
  this.c = 3;
};

var adapters = new Adapters();
adapters.add('test', {
  DocumentConstructor: AdapterDocumentConstructor,
});

var DocumentsFactory = require('ancient-document').AdaptersFactory;
var AdapterDocumentsFactory = require('ancient-document').AdapterFactory;

var FactoryDocumentConstructor = function() { this.b = 2; };
var documentsFactory = new DocumentsFactory(adapters, FactoryDocumentConstructor);

// Access from adapters
var documentsFactory = adapters.documentsFactory;

// Generate new document capsule
var document = documentsFactory.new("test", { _id: 123 });
document.data; // { _id: 123 };
document.a; // 1
document.b; // 2
document.c; // 3
```

## Versions

### 0.0.3
* Just adapters.documentsFactory option, no more AdaptersFactory/AdapterFactory.

### 0.0.2
* AdaptersFactory and AdapterFactory with example and test for adapter custom class.

### 0.0.1
* Support for new Adapters class.
* Timely document constructor initialization.

### 0.0.0
* Factory with global, factory and adapter prototypes #1