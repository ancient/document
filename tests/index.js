var assert = require('chai').assert;
var Factory = require('../index.js').Factory;
var Adapters = require('ancient-adapters').Adapters;

describe('ancient-document', function() {
  var adapters = new Adapters();
  adapters.add('test', {
    DocumentConstructor: function() {
      this.a = 1;
    }
  });
  
  var data = { abc: 123 };
  
  var factory = new Factory(adapters, function() { this.b = 2; });
  factory.documentPrototype.c = 3;
  
  var document;
  it('new document', function() {
    document = factory.new("test", data);
  });
  it('document.data', function() {
    assert.deepEqual(document.data, data);
  });
  it('globalDocumentPrototype', function() {
    assert.instanceOf(document, require('../index.js').DocumentConstructor);
  });
  it('factoryDocumentConstructor', function() {
    assert.instanceOf(document, factory.DocumentConstructor);
    assert.equal(document.b, 2);
  });
  it('factoryDocumentPrototype', function() {
    assert.equal(document.c, 3);
  });
  it('adapterDocumentConstructor', function() {
    assert.instanceOf(document, adapters.get('test').DocumentConstructor);
    assert.equal(document.a, 1);
  });
});